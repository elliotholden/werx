#!/usr/bin/perl
# Log parser - Elliot Holden (devopselliot@gmail.com)
# Takes an Apache log file as argument and returns the top 5 ip address or url matches in the file

use strict;
use warnings;

my $infile = $ARGV[0];
my $outfile = $infile . ".out";

open INFILE, "<", "$infile" or die "$!: Could not open file $infile\n";
my @lines = <INFILE>;
close INFILE;

my %hash;
my $count = 0;

my $regex = '([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})'; # use this to search for ip addresses
#my $regex = '"GET (\/.*) HTTP.*"'; # use this to search for urls

foreach my $line(@lines) {	
	chomp($line);
	if ($line !~ /^\s*$/ and $line =~ /$regex/) {
		$hash{$1} = $count unless exists $hash{$1};
		$hash{$1} = $hash{$1} + 1; 
   }
}

my $c = 1;

open OUTFILE, ">", "$outfile" or die "$!: Could not create file $outfile"; 
# Sort based on value instead of key
foreach my $key (sort { $hash{$b} <=> $hash{$a} } keys %hash) {
	print "Search String: $key\n" .  "Count: $hash{$key}\n\n";
	print OUTFILE "Search String: $key\n" .  "Count: $hash{$key}\n\n";
   last if $c++ == 5; # this works because`the incrementing is done after the comparison
}

close OUTFILE;

exit;
